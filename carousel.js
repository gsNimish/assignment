const CAROUSEL = document.querySelector('.carousel');
const SLIDES = CAROUSEL.querySelectorAll('.slide');
const MAX_SLIDES = SLIDES.length;
const SLIDE_CHANGE_INTERVAL = 3500;
let activeSlideIndex = MAX_SLIDES - 1;
let CAROUSEL_INT;
let indicators = true;
let NEXT = document.querySelector('.next');
let PREV = document.querySelector('.prev');

const nextSlide = () => {
	clearInterval(CAROUSEL_INT)
	changeSlide(1)
	autoSlide()
}
const prevSlide = () => {
	clearInterval(CAROUSEL_INT)
	changeSlide(-1)
	autoSlide()
}
NEXT.addEventListener('click', () => {
	nextSlide();
});
PREV.addEventListener('click', () => {
	prevSlide();
})
const genIndicators = () => {
	let html = document.createElement('div')
	html.classList.add('indicators')

	SLIDES.forEach((e, i) => {
		html.innerHTML += "<div class='indicator' onclick='slideTo(" + i + ")'></div>"
	})
	CAROUSEL.appendChild(html)
	return CAROUSEL.querySelectorAll('.indicator')
}
if (indicators && (MAX_SLIDES > 1))
	indicators = genIndicators()
else
	indicators = false
const slideTo = (index = 0) => {
	clearInterval(CAROUSEL_INT)
	SLIDES[activeSlideIndex].classList.remove('active-slide')
	indicators && indicators[activeSlideIndex].classList.remove('active')
	activeSlideIndex = getBoundedIndex(index, -1);
	changeSlide()
	autoSlide()
}
const getBoundedIndex = (index, dir) => {
	index = index == 0 ? MAX_SLIDES : index;
	index = (index + (1 * dir)) % MAX_SLIDES;
	return index;
}
const changeSlide = (dir = 1) => {
	SLIDES[activeSlideIndex].classList.remove('active-slide')
	indicators && indicators[activeSlideIndex].classList.remove('active')
	activeSlideIndex = getBoundedIndex(activeSlideIndex, dir)
	SLIDES[activeSlideIndex].classList.add('active-slide')
	indicators && indicators[activeSlideIndex].classList.add('active')

}

const autoSlide = () => {
	if (MAX_SLIDES > 1)
		CAROUSEL_INT = setInterval(changeSlide, SLIDE_CHANGE_INTERVAL)
}
changeSlide();
autoSlide();